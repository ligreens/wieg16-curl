<?php
function get_webpage($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}


$words = [
    "cheese" => 0,
    "lorem" => 0,
    "cookie" => 0
];

function count_words($text, array $needles)
{
    $text = strip_tags($text);
    $text = strtolower($text);

    $result = [];
    foreach ($needles as $word => $value) {
        $number = substr_count($text, $word, 0);
        $result[] = $word . " - " . $number;
    };
    return $result;
}

$page = get_webpage("http://www.cheeseipsum.co.uk/");

?>
<ul>
    <?php foreach (count_words($page, $words) as $count): ?>
        <li><?= $count ?></li>
    <?php endforeach; ?>
</ul>

